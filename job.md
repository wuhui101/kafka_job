## 一. 需求
    
    使用kafka做日志收集
    用户点击页面时，会将请求发往nginx，nginx收集用户请求发往kafka主题
    需要收集的信息：
        用户id(user_id)， 时间(act_time)， 操作(action, 可以是点击，收藏，投简历，上传简历)，对方企业编码

## 二. 实现思路
    1. 启动kafka，创建相应主题lg_log
    2. 配置nginx（nginx和nginx_kafka_moudle）
        参考资料：https://blog.csdn.net/qq_21918145/article/details/81585228
                  https://github.com/brg-liuwei/ngx_kafka_module
    3. 监听topic
    4. 使用 curl localhost/kafka/track -d "message send to kafka topic" 测试
    5. 编写html页面
    
## 三. 实现细节
    1. kafka
```
kafka-server-start.sh -daemon /opt/lagou/servers/kafka_2.12-1.0.2/config/server.properties
kafka-topics.sh --zookeeper localhost/kafka --topic lg_log --partitions 1 --replication-factor 1
```
    2. nginx
```
# 安装python3
  https://www.jianshu.com/p/e191f9dc1186
# 安装git
 yum install -y git

# 安装相关依赖
 yum install -y gcc gcc-c++ zlib zlib-devel openssl openssl-devel pcre pcre-devel

# kafka的客户端源码
cd /opt/lagou/software
git clone https://github.com/edenhill/librdkafka

# 编译
cd /opt/lagou/software/librdkafka 
./configure
make && make install

# 下载模块源码
 cd /opt/lagou/software
 git clone https://github.com/brg-liuwei/ngx_kafka_module

# 安装nginx
cd /opt/lagou/software
wget http://nginx.org/download/nginx-1.18.0.tar.gz
tar -zxf nginx-1.18.0.tar.gz

# 编译/opt/lagou/software/nginx-1.18.0
./configure --add-module=/root/software/ngx_kafka_module/
make && make install
```
    3. nginx配置
```
http {
    include       mime.types;
    default_type  application/octet-stream;

    kafka;
    kafka_broker_list 172.16.37.130:9092;


    #gzip  on;

    server {
        listen       80;
        server_name  172.16.37.130;

        location /kafka/log {
            kafka_topic lg_log;
        }
    }
}
```
    4. consumer监听

```
kafka-console-consumer.sh --bootstrap-server localhost:9092 --topic lg_log
```

    5. 测试
```
curl localhost/kafka/log -d "hello"
```
    
    6. html页面开发
<html>
 <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1,shrink-to-fit=no">
        <title>index</title>
        <!-- jquery cdn, 可换其他 -->
        <script src="https://cdn.bootcdn.net/ajax/libs/jquery/3.5.1/jquery.js"></script>
    </head>
    <body>
        <input id="click" type="button" value="点击" onclick="operate('click')" />
        <input id="collect" type="button" value="收藏" onclick="operate('job_collect')" />
        <input id="send" type="button" value="投简历" onclick="operate('cv_send')" />
        <input id="upload" type="button" value="上传简历" onclick="operate('cv_upload')" />
    </body>

    <script>

        function operate(action) {

            var json = {'user_id': 'u_donald', 'act_time': current().toString(), 'action': action, 'job_code': 'donald'};

            $.ajax({
                url:"http://172.16.64.21:8880/kafka/log",
                type:"POST" ,
                crossDomain: true,
                data: JSON.stringify(json),
                // 下面这句话允许跨域的cookie访问
                xhrFields: {
                    withCredentials: true
                },
                success:function (data, status, xhr) {

                    // console.log("操作成功：'" + action)
                },
                error:function (err) {

                    // console.log(err.responseText);
                }
            });
        };

        function current() {
            var d   = new Date(),
                str = '';
            str += d.getFullYear() + '-';
            str += d.getMonth() + 1 + '-';
            str += d.getDate() + ' ';
            str += d.getHours() + ':';
            str += d.getMinutes() + ':';
            str += d.getSeconds();
            return str;
        }
    </script>
</html>
